//
//  HideKeyvord.swift
//  Authentication
//
//  Created by Nirav Bhadani  on 12/04/18.
//  Copyright © 2018 Nirav Bhadani . All rights reserved.
//

import UIKit

class HideKeybord {
    
    var view = UIView()
    
    func hideKeybord(){
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HideKeybord.dismissKeybord))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeybord() {
        
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
}

