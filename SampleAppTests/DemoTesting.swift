//
//  DemoTesting.swift
//  TestingAppTests
//
//  Created by Nirav Bhadani  on 13/04/18.
//  Copyright © 2018 Nirav Bhadani . All rights reserved.
//

import XCTest
@testable import SampleApp


class DemoTesting: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMailValidation() {
        
        let mailValidation = MailValidation()
        let validMail = "ab@c.com"
        let invalidmail = "AB@.com"
        let invalidPass = "12341213156146541156ffsdf"
        let empty = ""
        
        XCTAssertTrue(mailValidation.isValidEmail(mail: validMail))
        XCTAssertFalse(mailValidation.isValidEmail(mail: invalidmail))
        XCTAssertFalse(mailValidation.ispasswordValid(password: invalidPass))
        XCTAssertTrue(mailValidation.ispasswordValid(password: invalidmail))
        XCTAssertTrue(mailValidation.ispasswordValid(password: invalidmail))
        XCTAssertFalse(mailValidation.ismailandpassEmpty(mail: empty, password: empty))
        XCTAssertTrue(mailValidation.ismailandpassEmpty(mail: invalidmail, password: invalidPass))
        
        
    }
}
