//
//  AlertVC.swift
//  Authentication
//
//  Created by Nirav Bhadani  on 12/04/18.
//  Copyright © 2018 Nirav Bhadani . All rights reserved.
//

import UIKit

class AlertView {
    
    func alertView (title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        return alert
    }
}
