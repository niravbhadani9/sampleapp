//
//  mailvalidation.swift
//  Authentication
//
//  Created by Nirav Bhadani  on 12/04/18.
//  Copyright © 2018 Nirav Bhadani . All rights reserved.
//

import UIKit


class MailValidation {
    
    func ismailandpassEmpty (mail: String, password: String) -> Bool {
        
        if mail.isEmpty == true || password.isEmpty == true {
            
            return false
        }else {
            
            return true
        }
    }
  
    func isValidEmail (mail : String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: mail)
    }
    
    func ispasswordValid (password : String) -> Bool {
        
        if password.count < 6 || password.count > 15 {
            
            return false
        }
        return true
    }
}
