//
//  SignUPVC.swift
//  Authentication
//
//  Created by Nirav Bhadani  on 12/04/18.
//  Copyright © 2018 Nirav Bhadani . All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class SignUPVC: UIViewController {

    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpBtn: UIButton!
    
    let alertView = AlertView()
    let dismissKeybord = HideKeybord()
    let mailValidation = MailValidation()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signUpBtn.layer.cornerRadius = 7
        dismissKeybord.view = self.view
        dismissKeybord.hideKeybord()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        mailTextField.text = ""
        passwordTextField.text = ""
        
    }
    
    @IBAction func signUpBtnPressed(_ sender: UIButton) {
        
        let ismailEmpty = self.mailValidation.ismailandpassEmpty(mail: mailTextField.text! , password: passwordTextField.text!)
        let isMailValid = self.mailValidation.isValidEmail(mail: mailTextField.text!)
        let isPassValid = self.mailValidation.ispasswordValid(password: passwordTextField.text!)

        
        func signUp() {
            
            SVProgressHUD.show()
            
            Auth.auth().createUser(withEmail: mailTextField.text!, password: passwordTextField.text!, completion: { (user, error) in
                
                if error != nil {
                    
                    print(error!)
                    let alert = self.alertView.alertView(title: "Error", message: "Plese Check your mail or password are not correct")
                    SVProgressHUD.dismiss()
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }else {
                    
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "toAppVC", sender: self)
                }
            })
        }
        
        if ismailEmpty == false {
            
            let alert = self.alertView.alertView(title: "", message: "Plese Fill Both Mail and Password.")
            self.present(alert, animated: true, completion: nil)
            
        }else if isMailValid == false {
            
            let alert = self.alertView.alertView(title: "", message: "Invalid Mail Plese chack Again and Reenter It.")
            self.present(alert, animated: true, completion: nil)
            
        }else if isPassValid == false {
            
            let alert = self.alertView.alertView(title: "", message: "Plese Enter PassWord of Lenth 6 to 15 Charecters.")
            self.present(alert, animated: true, completion: nil)
            
        }else {
            
            signUp()
        }
    }
}
