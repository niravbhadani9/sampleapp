//
//  MainVC.swift
//  Authentication
//
//  Created by Nirav Bhadani  on 12/04/18.
//  Copyright © 2018 Nirav Bhadani . All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class MainVC: UIViewController {

    
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    @IBOutlet weak var logInBtn : UIButton!
    @IBOutlet weak var signupBtn : UIButton!
    
    let alertView = AlertView()
    let hideKeyBord = HideKeybord()
    let mailValidation = MailValidation()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyBord.view = self.view
        hideKeyBord.hideKeybord()
        logInBtn.layer.cornerRadius = 7
        signupBtn.layer.cornerRadius = 7
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        mailTextField.text = ""
        passWordTextField.text = ""
    }

    @IBAction func logInBtnPtessed(_ sender: UIButton) {
        
        let ismailEmpty = self.mailValidation.ismailandpassEmpty(mail: mailTextField.text!, password: passWordTextField.text!)
        
        
        func login() {
            
            SVProgressHUD.show()
            
            
            Auth.auth().signIn(withEmail: mailTextField.text!, password: passWordTextField.text!, completion: { (user, error) in
                
                if error != nil {
                    
                    let alert = self.alertView.alertView(title: "Error", message: "Somthing is Wrong. Plese Check Your Mail and Password. If you haven't SignIn Befor plese Do SignUp")
                    SVProgressHUD.dismiss()
                    self.present(alert, animated: true, completion: nil)
                    
                }else {
                    
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "toAppVC", sender: self)
                }
            })
        }
        
        if ismailEmpty == false {
            
            let alert = self.alertView.alertView(title: "", message: "Plese Fill Email and Password.")
            self.present(alert, animated: true, completion: nil)
            
        }else {
            
            login()
        }
    }
}
